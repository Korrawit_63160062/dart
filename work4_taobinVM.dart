import 'dart:io';

class taobinVendingMachine {
  int price = 0;
  int inputMoney = 0;

  showWelcome() {
    print("Welcome");
  }

  showCategories() {
    print("Please select a drink type.");
    print("1. Coffee");
    print("2. Tea");
    print("3. Cocoa");
    print("4. Soda");
  }

  checkCategorties(int selection) {
    while (selection > 4 || selection < 1) {
      print("Invalid input. Please try again.");
      showCategories();
      int selection = int.parse(stdin.readLineSync()!);
      checkCategorties(selection);
    }
    switch (selection) {
      case 1:
        {
          coffee temp = new coffee();
          showType();
          int type = int.parse(stdin.readLineSync()!);
          type = inputType(type);
          temp.setType(type);
          showSugarLevel();
          int sugarLevel = int.parse(stdin.readLineSync()!);
          sugarLevel = inputSugarLevel(sugarLevel);
          temp.setSugarLevel(sugarLevel);
          askStraw();
          int straw = int.parse(stdin.readLineSync()!);
          straw = inputStraw(straw);
          temp.setStraw(straw);
          askCupCover();
          int cupCover = int.parse(stdin.readLineSync()!);
          cupCover = inputCupCover(cupCover);
          temp.setCupCover(cupCover);
          price = temp.getPrice();
          temp.getorderDetail();
          print("Your total price is $price");
        }
        break;
      case 2:
        {
          tea temp = new tea();
          showType();
          int type = int.parse(stdin.readLineSync()!);
          type = inputType(type);
          temp.setType(type);
          showSugarLevel();
          int sugarLevel = int.parse(stdin.readLineSync()!);
          sugarLevel = inputSugarLevel(sugarLevel);
          temp.setSugarLevel(sugarLevel);
          askStraw();
          int straw = int.parse(stdin.readLineSync()!);
          straw = inputStraw(straw);
          temp.setStraw(straw);
          askCupCover();
          int cupCover = int.parse(stdin.readLineSync()!);
          cupCover = inputCupCover(cupCover);
          temp.setCupCover(cupCover);
          price = temp.getPrice();
          temp.getorderDetail();
          print("Your total price is $price");
        }
        break;
      case 3:
        {
          cocoa temp = new cocoa();
          showType();
          int type = int.parse(stdin.readLineSync()!);
          type = inputType(type);
          temp.setType(type);
          showSugarLevel();
          int sugarLevel = int.parse(stdin.readLineSync()!);
          sugarLevel = inputSugarLevel(sugarLevel);
          temp.setSugarLevel(sugarLevel);
          askStraw();
          int straw = int.parse(stdin.readLineSync()!);
          straw = inputStraw(straw);
          temp.setStraw(straw);
          askCupCover();
          int cupCover = int.parse(stdin.readLineSync()!);
          cupCover = inputCupCover(cupCover);
          temp.setCupCover(cupCover);
          price = temp.getPrice();
          temp.getorderDetail();
          print("Your total price is $price");
        }
        break;
      case 4:
        {
          soda temp = new soda();
          showSugarLevel();
          int sugarLevel = int.parse(stdin.readLineSync()!);
          sugarLevel = inputSugarLevel(sugarLevel);
          temp.setSugarLevel(sugarLevel);
          askStraw();
          int straw = int.parse(stdin.readLineSync()!);
          straw = inputStraw(straw);
          temp.setStraw(straw);
          askCupCover();
          int cupCover = int.parse(stdin.readLineSync()!);
          cupCover = inputCupCover(cupCover);
          temp.setCupCover(cupCover);
          price = temp.getPrice();
          temp.getorderDetail();
          print("Your total price is $price");
        }
        break;
    }
    getMoney();
  }

  int inputCupCover(int cupCover) {
    while (cupCover > 2 || cupCover < 1) {
      print("Invalid input. Please try again.");
      askCupCover();
      cupCover = int.parse(stdin.readLineSync()!);
    }
    return cupCover;
  }

  int inputStraw(int straw) {
    while (straw > 2 || straw < 1) {
      print("Invalid input. Please try again.");
      askStraw();
      straw = int.parse(stdin.readLineSync()!);
    }
    return straw;
  }

  int inputSugarLevel(int sugarLevel) {
    while (sugarLevel > 5 || sugarLevel < 1) {
      print("Invalid input. Please try again.");
      showSugarLevel();
      sugarLevel = int.parse(stdin.readLineSync()!);
    }
    return sugarLevel;
  }

  int inputType(int type) {
    while (type > 3 || type < 1) {
      print("Invalid input. Please try again.");
      showType();
      type = int.parse(stdin.readLineSync()!);
    }
    return type;
  }

  askCupCover() {
    print("Do you need cup cover");
    print("1. Yes");
    print("2. No");
  }

  askStraw() {
    print("Do you need straw");
    print("1. Yes");
    print("2. No");
  }

  showSugarLevel() {
    print("Please select sugar level");
    print("1. Lowest");
    print("2. Low");
    print("3. Medium");
    print("4. High");
    print("5. Highest");
  }

  showType() {
    print("Please select your drink type");
    print("1. Cool");
    print("2. Hot ( + 2 bath )");
    print("3. Mix ( + 5 bath )");
  }

  getMoney() {
    print("Please input your money");
    int money = int.parse(stdin.readLineSync()!);
    inputMoney = inputMoney + money;
    price = price - money;
    int remain = price;
    if (remain > 0) {
      int remain = price;
      print("Your input money : $inputMoney");
      print("Remaining : $remain");
      getMoney();
    } else if (remain < 0) {
      int exchange = remain - remain - remain;
      print("Exchange : $exchange");
      print("Thank you");
    } else {
      print("Thank you");
    }
  }

  getPrice() {
    return price;
  }

  getInputMoney() {
    return inputMoney;
  }
}

abstract class drink {
  int price = 0;
  String type = "Default"; // Cool , Hot , Mix
  String sugarLevel = "Default"; // Lowest , Low , Medium , High , Highest
  String straw = "Default"; // Yes , No
  String cupCover = "Default"; // Yes , No

  setSugarLevel(int selection) {
    switch (selection) {
      case 1:
        {
          sugarLevel = "Lowest";
        }
        break;
      case 2:
        {
          sugarLevel = "Low";
        }
        break;
      case 3:
        {
          sugarLevel = "Medium";
        }
        break;
      case 4:
        {
          sugarLevel = "High";
        }
        break;
      case 5:
        {
          sugarLevel = "Highest";
        }
        break;
    }
  }

  setStraw(int selection) {
    switch (selection) {
      case 1:
        {
          straw = "Yes";
        }
        break;
      case 2:
        {
          straw = "No";
        }
        break;
    }
  }

  setCupCover(int selection) {
    switch (selection) {
      case 1:
        {
          cupCover = "Yes";
        }
        break;
      case 2:
        {
          cupCover = "No";
        }
        break;
    }
  }

  getorderDetail() {
    print("Order detail");
    print("Drink type : $type");
    print("Sugar level : $sugarLevel");
    print("Straw : $straw");
    print("Cup cover : $cupCover");
  }
}

abstract class ToSetType {
  setType(int variable) {
    // Add code here.
  }
}

class coffee extends drink implements ToSetType {
  @override
  int price = 40;

  @override
  setType(int selection) {
    switch (selection) {
      case 1:
        {
          type = "Cool";
        }
        break;
      case 2:
        {
          type = "Hot";
          price = price + 2;
        }
        break;
      case 3:
        {
          type = "Mix";
          price = price + 5;
        }
        break;
    }
  }

  int getPrice() {
    return price;
  }
}

class tea extends drink implements ToSetType {
  int price = 45;

  @override
  setType(int selection) {
    switch (selection) {
      case 1:
        {
          type = "Cool";
        }
        break;
      case 2:
        {
          type = "Hot";
          price = price + 5;
        }
        break;
      case 3:
        {
          type = "Mix";
          price = price + 5;
        }
        break;
    }
  }

  int getPrice() {
    return price;
  }
}

class cocoa extends drink implements ToSetType {
  int price = 50;

  @override
  setType(int selection) {
    switch (selection) {
      case 1:
        {
          type = "Cool";
        }
        break;
      case 2:
        {
          type = "Hot";
          price = price + 4;
        }
        break;
      case 3:
        {
          type = "Mix";
          price = price + 7;
        }
        break;
    }
  }

  int getPrice() {
    return price;
  }
}

class soda extends drink {
  @override
  String type = "Default";
  int price = 40;

  int getPrice() {
    return price;
  }
}
void main() {
  taobinVendingMachine taobin1 = new taobinVendingMachine();
  taobin1.showWelcome();
  taobin1.showCategories();
  int categoriesSelection = int.parse(stdin.readLineSync()!);
  taobin1.checkCategorties(categoriesSelection);
}
