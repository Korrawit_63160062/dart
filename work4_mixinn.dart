import 'dart:io';

mixin Swim {
  void swim() => print('Swimming');
}

mixin Bite {
  void bite() => print('Chomp');
}

mixin Crawl {
  void crawl() => print('Crawling');
}

abstract class Reptile with Swim, Bite, Crawl{}

class Alligator extends Reptile {
  void hunt() {
    print('Alligator -------');
    swim();
    crawl();
    bite();
    print('Eat Fish');
  }
}

class Crocodile extends Reptile {
  void hunt() {
    print('Crocodile -------');
    swim();
    crawl();
    bite();
    print('Eat Zebra');
  }
}

class Fish with Swim, Bite{}

void main() {
  var ali = Alligator();
  var cro = Crocodile();

  ali.hunt();
  cro.hunt();
}
