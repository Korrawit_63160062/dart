import 'dart:io';

void hotPotato(var children, int K, int potato) {
        if (children.length() == 1) {
            print(children);
            return;
        }
        int length = children.length();
        potato = ((potato + K) %  length);
        print(children.get(potato));
        children.remove(potato);
        print(children);

        hotPotato(children, K, potato);
}

void main() {
var children = [];
        print("Input N : ");
        int N = int.parse(stdin.readLineSync()!);
        print("Input K : ");
        int K = int.parse(stdin.readLineSync()!);
        int potato = 0;

        for (int i = 1; i <= N; i++) {
            children.add(i);
        }
        print("Player $children");
        hotPotato(children, K, potato);
}