import 'dart:io';

double add(double x, double y) {
  double result = x + y;
  return result;
}

double substract(double x, double y) {
  double result = x - y;
  return result;
}

double mutiply(double x, double y) {
  double result = x * y;
  return result;
}

double divide(double x, double y) {
  double result = x / y;
  return result;
}

void main() {
  int operator;
  bool exit = false;
  double x = 0, y = 0;
  while (exit == false) {
    print('MENU \nSelect the choice you want to perform :\n1. ADD\n2. SUBSTRACT\n3. MULTIPLY\n4. DIVIDE\n5. EXIT\nChoice you want to enter :');
    operator = int.parse(stdin.readLineSync()!);

    void getInput() {
      print('Enter the value for x :');
      x = double.parse(stdin.readLineSync()!);
      print('Enter the value for y :');
      y = double.parse(stdin.readLineSync()!);
    }

    switch (operator) {
      case 1:
        {
          getInput();
          double answer = add(x, y);
          print('Sum of the two numbers is :');
          print('$answer');
        }
        break;
      case 2:
        {
          getInput();
          double answer = substract(x, y);
          print('Difference of the two numbers is :');
          print('$answer');
        }
        break;
      case 3:
        {
          getInput();
          double answer = mutiply(x, y);
          print('Multiply of the two numbers is :');
          print('$answer');
        }
        break;
      case 4:
        {
          getInput();
          double answer = divide(x, y);
          print('Quotient of the two numbers is :');
          print('$answer');
        }
        break;
      case 5:
        {
          exit = true;
        }
        break;
      default:
        {
          print('Invalid choice');
        }
        break;
    }
  }
}
