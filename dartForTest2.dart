class calculator {
  double a = 0;
  double b = 0;

  calculator(double a, double b) {
    this.a = a;
    this.b = b;
  }

  double add() {
    this.a = a;
    this.b = b;
    return this.a + this.b;
  }

  double substract() {
    this.a = a;
    this.b = b;
    return this.a - this.b;
  }

  double multiply() {
    this.a = a;
    this.b = b;
    return this.a * this.b;
  }

  double divide() {
    this.a = a;
    this.b = b;
    return this.a / this.b;
  }

  double modulo() {
    this.a = a;
    this.b = b;
    return this.a % this.b;
  }

  double exponent() {
    this.a = a;
    this.b = b;
    double expo = a;
    double result = 1;
    for (int i = 0; i < b; i++) {
      result = result * expo;
    }
    return result;
  }
}

void main() {
  calculator a1 = new calculator(5,4);

  double sum = a1.add();
  double substract = a1.substract();
  double multiply = a1.multiply();
  double divide = a1.divide();
  double modulo = a1.modulo();
  double exponent = a1.exponent();
  
  print('sum = $sum');
  print('substract = $substract');
  print('multiply = $multiply');
  print('divide = $divide');
  print('modulo = $modulo');
  print('exponent = $exponent');
}
