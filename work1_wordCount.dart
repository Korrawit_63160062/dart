import "dart:io";

void main() {
  print('Input your paragraph :');
  String paragraph = stdin.readLineSync() as String;
  paragraph = paragraph.toLowerCase();

  paragraph = paragraph.replaceAll(".", "");
  paragraph = paragraph.replaceAll(",", "");
  paragraph = paragraph.replaceAll("-", "");
  paragraph = paragraph.replaceAll("!", "");
  paragraph = paragraph.replaceAll("'", "");
  paragraph = paragraph.replaceAll("‘", "");
  paragraph = paragraph.replaceAll("’", "");
  paragraph = paragraph.replaceAll(";", "");
  paragraph = paragraph.replaceAll("(", "");
  paragraph = paragraph.replaceAll(")", "");
  paragraph = paragraph.replaceAll("[", "");
  paragraph = paragraph.replaceAll("]", "");
  paragraph = paragraph.replaceAll("{", "");
  paragraph = paragraph.replaceAll("}", "");

  var list = paragraph.split(" ");
  var uniqueList = list.toSet().toList();

  Map map = {};
  list.forEach((element) {
    if (map.containsKey(element)) {
      map[element] = map[element] + 1;
    } else {
      map[element] = 1;
    }
  });
  int uniqueWord = map.length;
  print("Found $uniqueWord unique words.");
  map.forEach((key, value) {
    print("Found \"" + key.toString() + "\" " + value.toString() + " times.");
  });
  print("Paragraph after remove duplicate words: \""+uniqueList.join(' ')+"\"");
}
